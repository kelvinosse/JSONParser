namespace JSONParser
{
    public enum TokenType
    {
        OpenCurlyBracket,
        CloseCurlyBracket,
        OpenSquareBracket,
        CloseSquareBracket,
        DoubleQuote,
        Colon,
        Comma,
        Number,
        Hyphen,
        Period,
        Escape,
        PrettyToken,
        Unknown
    }
}