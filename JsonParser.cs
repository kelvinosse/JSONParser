using System;
using System.Collections.Generic;

namespace JSONParser
{
    public class JsonParser
    {
        public JsonParser(){
        }
        public object ParseIt(string source)
		{
			return new JsonSyntaxAnalyzer(source).Parse();
		}
    }
}