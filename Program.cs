using System;
using System.IO;
namespace JSONParser
{
    public class Program
    {
        public static void Main(string[] args){
            while(true){
                Console.WriteLine("\nType a path to a valid JSON file or type quit\n");
                string input = Console.ReadLine();
                if(input == "quit") break;
                string data = "";
                try{
                    if(!input.EndsWith("json")) data = File.ReadAllText(input + ".json");
                    else data = File.ReadAllText(input);
                }catch(FileNotFoundException e){
                    Console.BackgroundColor = ConsoleColor.DarkRed;
                    Console.WriteLine(e.Message);
                    Console.ResetColor();
                }
                JsonParser parser = new JsonParser();
                try{
                    parser.ParseIt(data);
                    Console.WriteLine("Json is valid!");
                }catch(SyntaxException e){
                    Console.BackgroundColor = ConsoleColor.DarkRed;
                    Console.WriteLine(e.Message);
                    Console.ResetColor();
                }
            }
        }
    }
}